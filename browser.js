var gui = require('nw.gui');
var win = gui.Window.get();
var searchEngine;
var incognito = false;
var ctrlChk = false;
var fs = require('fs');
var urlCheck = true;
var maximized = false;
var currentTab = 1;
var isTitle = true;
var plgs = [];
var plgCounter = 0;

//CODE AND BROWSER BY RESTMONR

function applyAll() {
    getSearch();
	plgLoad();
	loadbkm();
	setTheme('loadit');
	refresh();
}

win.on('new-win-policy', function (frame, url, policy) {
	if (url != window.top.location) {
	policy.forceCurrent();
	}
});

function maximinimize() {
	if (maximized == false) {
		win.maximize();
		maximized = true;
	} else {
		win.unmaximize();
		maximized = false;
	}
}

function unIncognito() {
	if (incognito == true) {
		incognito = false;
		alert('Запись истории включена');
	} else {
		incognito = true;
		alert('Запись истории отключена');
	}
}

function whTurner() {
	if (incognito == false) {
		var tlt = document.getElementById('browser').contentWindow.document.title;
		var txt;
		try {
		txt = fs.readFileSync(gui.App.dataPath + '/NULLhstr.ini', 'utf-8');
		} catch(a) {
			txt = '';
		}
		var current = document.getElementById('browser').contentWindow.location;
		var toWrite = '<a href=\"' + current + '\" target=\"brws\" class=\"a\" onclick=\"clAll()\" oncontextmenu=\"removeHistoryItem(this)\">' + tlt + '</a>' + txt;
		fs.writeFileSync(gui.App.dataPath + '/NULLhstr.ini', toWrite);
	}
	showHistory();
}

function showHistory() {
	var txt;
	try {
	txt = fs.readFileSync(gui.App.dataPath + '/NULLhstr.ini', 'utf-8');
	} catch(a) {
		txt = '';
	}
	document.getElementById('hstrHere').innerHTML = txt;
}

function editBkms() {
	gui.Shell.openItem(document.getElementById('bkF').contentWindow.location);
}

function resetSettings() {
		fs.writeFileSync(gui.App.dataPath + '/NULLsearch.ini', '');
		fs.writeFileSync(gui.App.dataPath + '/NULLbkms.ini', '');
		fs.writeFileSync(gui.App.dataPath + '/NULLconfig.ini', '');
		fs.writeFileSync(gui.App.dataPath + '/NULLplg.ini', '');
		fs.writeFileSync(gui.App.dataPath + '/NULLhstr.ini', '');
		alert('Для применения сброса перезагрузите браузер!');
}


function exportSettings() {
	var txt1, txt2, txt3, txt4, txt5;
	try {
	txt1 = fs.readFileSync(gui.App.dataPath + '/NULLsearch.ini', 'utf-8');
	} catch(a) {txt1 = '';}
	try {
	txt2 = fs.readFileSync(gui.App.dataPath + '/NULLbkms.ini', 'utf-8');
	} catch(a) {txt2 = '';}
	try {
	txt3 = fs.readFileSync(gui.App.dataPath + '/NULLconfig.ini', 'utf-8');
	} catch(a) {txt3 = '';}
	try {
	txt4 = fs.readFileSync(gui.App.dataPath + '/NULLplg.ini', 'utf-8');
	} catch(a) {txt4 = '';}
	try {
	txt5 = fs.readFileSync(gui.App.dataPath + '/NULLhstr.ini', 'utf-8');
	} catch(a) {txt5 = '';}
	var path = prompt('Путь до папки, куда нужно произвести экспорт настроек', '');
	if (path) {
		fs.writeFileSync(path + '/NULLsearch.ini', txt1);
		fs.writeFileSync(path + '/NULLbkms.ini', txt2);
		fs.writeFileSync(path + '/NULLconfig.ini', txt3);
		fs.writeFileSync(path + '/NULLplg.ini', txt4);
		fs.writeFileSync(path + '/NULLhstr.ini', txt5);
		alert('Экспорт произведен!');
	} else {
		alert('Ошибка! Путь не введен!');
	}
}

function importSettings() {
	var path = prompt('Путь до папки, откуда нужно произвести импорт настроек', '');
	var check = false;
	var txt1, txt2, txt3, txt4, txt5;
	try {
	txt1 = fs.readFileSync(path + '/NULLsearch.ini', 'utf-8');
	} catch(a) {txt1 = '';}
	try {
	txt2 = fs.readFileSync(path + '/NULLbkms.ini', 'utf-8');
	} catch(a) {txt2 = '';}
	try {
	txt3 = fs.readFileSync(path + '/NULLconfig.ini', 'utf-8');
	} catch(a) {txt3 = '';}
	try {
	txt4 = fs.readFileSync(path + '/NULLplg.ini', 'utf-8');
	} catch(a) {txt4 = '';}
	try {
	txt5 = fs.readFileSync(path + '/NULLhstr.ini', 'utf-8');
	} catch(a) {txt5 = '';}
	if (path) {		
		fs.writeFileSync(gui.App.dataPath + '/NULLsearch.ini', txt1);
		fs.writeFileSync(gui.App.dataPath + '/NULLbkms.ini', txt2);
		fs.writeFileSync(gui.App.dataPath + '/NULLconfig.ini', txt3);
		fs.writeFileSync(gui.App.dataPath + '/NULLplg.ini', txt4);
		fs.writeFileSync(gui.App.dataPath + '/NULLhstr.ini', txt5);
		alert('Для применения настроек перезагрузите браузер!');
	} else {
		alert('Ошибка! Путь не введен!');
	}
}

function plgLoad() {
	var txt;
	try {
		txt = fs.readFileSync(gui.App.dataPath + '/NULLplg.ini', 'utf-8');
	} catch(a) {
		txt = '';
	}
	plgs = txt.split(', ');
	var container = document.getElementById('plgHere');
	var displayContainer = document.getElementById('plgDisplayHere');
	container.innerHTML = '';
	displayContainer.innerHTML = '';
	for (plgCounter = 0; plgCounter < plgs.length; plgCounter++) {
		var current = document.createElement('script');
		var toAdd = plgs[plgCounter];	
		current.src = toAdd;
		container.appendChild(current);
	}
	//
	for (var i = 0; i < plgs.length; i++) {
		if (plgs[i]) {
		displayContainer.innerHTML = displayContainer.innerHTML + '<a oncontextmenu=\"plgRemove(this)\" number=\"' + i + '\">' + plgs[i] + '</a>';
		}
	}
}

function plgAdd() {
	var toAdd = document.getElementById('plgPath').value;
	plgCounter++;
	plgs[plgCounter] = toAdd;
	var txt;
	try {
		txt = fs.readFileSync(gui.App.dataPath + '/NULLplg.ini', 'utf-8');
	} catch(a) {
		txt = '';
	}
	fs.writeFileSync(gui.App.dataPath + '/NULLplg.ini', txt + toAdd + ', ');
	plgLoad();
}

function plgRemove(arg) {
	var toDelete = arg.getAttribute('number');
	plgs.splice(toDelete, 1);
	var toWrite = plgs.join(', ');
	fs.writeFileSync(gui.App.dataPath + '/NULLplg.ini', toWrite);
	plgLoad();
}

function getSearch() {
	var rawSearchEngine;
	try {
	rawSearchEngine = fs.readFileSync(gui.App.dataPath + '/NULLsearch.ini', 'utf-8');
	} catch(a) {
		rawSearchEngine = 'http://google.com/';
	}
	
	if (rawSearchEngine == '') {
		rawSearchEngine = 'http://google.com/';
	}
	
	if (rawSearchEngine[rawSearchEngine.length-1] != '/') {
		rawSearchEngine = rawSearchEngine + '/';
	}
	
	if (rawSearchEngine.indexOf('://')+1) { } else {
		rawSearchEngine = 'http://' + rawSearchEngine;
	}
		
	searchEngine = rawSearchEngine;
	document.getElementById('seName').value = searchEngine;
	fs.writeFileSync('NULLsearch.html', searchEngine);
}

function setSearch() {
	var searchValue = document.getElementById('seName').value;
	fs.writeFileSync(gui.App.dataPath + '/NULLsearch.ini', searchValue);
	getSearch();
}

function browse() {
	var urll = document.form1.url.value;
	var browser = document.getElementById('browser');
	
	if (urll == 'about:blank' || urll == 'ABOUT:BLANK') {
		browser.src = 'pgs/start.html';
	} else if (urll.indexOf('://')+1) {
		browser.contentWindow.location = urll;
	} else {
		if (urll.indexOf('.')+1 && !(urll.indexOf(' ')+1) ) {
			browser.contentWindow.location = 'http://' + urll;
		} else {
			searchit();
		}
	}
}

function refresh() {
	document.getElementById('browser').contentWindow.location.reload(true);
	document.getElementById('browser').contentWindow.location.reload(true);
}

function devtools() {
	win.showDevTools('browser');
}

function loadbkm() {
	var txt;
	try {
	txt = fs.readFileSync(gui.App.dataPath + '/NULLbkms.ini', 'utf-8');
	} catch(a) {
		txt = '';
	}
	document.getElementById('bkmsHere').innerHTML = txt;
}

function updtitle() {
	if (document.getElementById('browser').contentWindow.location == 'data:text/html,chromewebdata' || document.getElementById('browser').contentWindow.location == 'about:blank') {
		document.getElementById('browser').src = 'pgs/404.html';
	}
		document.form1.url.value = document.getElementById('browser').contentWindow.location; 
}

function custombkm() {
	var siteAdress = prompt('Адрес сайта :', '');
	var siteName = prompt('Имя сайта :', '');
	
	if (siteAdress.indexOf('://')+1) {} else {siteAdress = 'http://' + siteAdress;}
	
	if (siteAdress && siteName) {
	var txt;
	try {
	txt = fs.readFileSync(gui.App.dataPath + '/NULLbkms.ini', 'utf-8');
	} catch(a) {
		txt = '';
	}
	fs.writeFileSync(gui.App.dataPath + '/NULLbkms.ini', txt + '<a href=\"' + siteAdress + '\" target=\"brws\" class=\"a\" onclick=\"clAll()\" oncontextmenu=\"removeBkmItem(this)\">' + siteName + '</a>');
	} else {
		alert('Ошибка! Введено не все!');
	}
	loadbkm();
}

function addbkm() {
	var txt;
	try {
	txt = fs.readFileSync(gui.App.dataPath + '/NULLbkms.ini', 'utf-8');
	} catch(a) {
		txt = '';
		document.getElementById('bkF').contentWindow.location.reload(true);
	}
	var loc = document.getElementById('browser').contentWindow.location;
	var tlt = document.getElementById('browser').contentWindow.document.title;
	fs.writeFileSync(gui.App.dataPath + '/NULLbkms.ini', txt + '<a href=\"' + loc + '\" target=\"brws\" class=\"a\" onclick=\"clAll()\" oncontextmenu=\"removeBkmItem(this)\">' + tlt + '</a>');
	loadbkm();
}

function removeBkmItem(arg) {
	arg.remove();
	fs.writeFileSync(gui.App.dataPath + '/NULLbkms.ini', document.getElementById('bkmsHere').innerHTML);
}

function removeHistoryItem(arg) {
	arg.remove();
	fs.writeFileSync(gui.App.dataPath + '/NULLhstr.ini', document.getElementById('hstrHere').innerHTML);
}

function resetbkm() {
	fs.writeFileSync(gui.App.dataPath + '/NULLbkms.ini', '');
	loadbkm();
}

function resethstr() {
	fs.writeFileSync(gui.App.dataPath + '/NULLhstr.ini', '');
	showHistory();
}

function backit() {
	document.getElementById('browser').contentWindow.history.back();
}

function forwardit() {
	document.getElementById('browser').contentWindow.history.forward();
}

function keys() {
	var key = event.keyCode;
	if (key == 116) {
		refresh();
	}
	
	if (key == 13) {
		browse();
	}
	
	if (key == 120) {
		searchit();
	}
	
	if (key == 123) {
		devtools();
	}
	
	if (key == 17) {
		ctrlChk = true;
	}
	
//
	if (ctrlChk == true) {
		if (key == 66) {
			selectIt('other');
		}
		
		if (key == 78){
			window.open('index.html');
		}
		
		if (key == 36) {
			document.getElementById('browser').src = 'pgs/start.html';
		}
		
		if (key == 77) {
			searchit();
		}
		
		if (key == 37) {
			backit();
		}
		
		if (key == 39) {
			forwardit();
		}
		
		if (key == 79) {
			var fileName = prompt('Введите полный путь до файла для открытия', '');
			if (fileName != null && fileName != '') {
				document.getElementById('browser').contentWindow.location = 'file:///' + fileName;
			} else {
				alert('Ошибка! Путь не введен!');
			}
		}
	}
//
}

function ctrlKeys() {
	var key = event.keyCode;
	if (key == 17) {
		ctrlChk = false;
	}
}

function searchit() {
	var svalue = document.form1.url.value;
	var browser = document.getElementById('browser');
	var a = searchEngine + '?q=' + svalue;
	browser.contentWindow.location = a;
}

function clAll() {
	var tabs = document.getElementsByClassName('tab');
	var i;
	for (i = 0; i < tabs.length; i++) {
		tabs[i].style.display = 'none';
	}
	document.getElementById('other').style.display = 'none';
}

function clOther() {
	var tabs = document.getElementsByClassName('tab');
	var checked = true;
	for (i = 0; i < tabs.length; i++) {
		if (tabs[i].style.display == 'none') {} else {
			checked = false;
			break;
		}
	}
	if (checked) {
		unhoverit('other');
	}
}

function selectIt(arg, arg2, arg3) {
	var obj = document.getElementById(arg);
	var obj2 = document.getElementById(arg2);
	var obj3 = document.getElementById(arg3);
	var tabs = document.getElementsByClassName('tab');
	var i;
	for (i = 0; i < tabs.length; i++) {
		tabs[i].style.display = 'none';
	}
	obj.style.display = 'block';
	obj2.style.display = 'block';
	obj3.style.display = 'block';
}

function unhoverit(arg) {
	var obj = document.getElementById(arg);
	obj.style.display = 'none';
}

function hoverit(arg) {
	var obj = document.getElementById(arg);
	obj.style.display = 'block';
}

//Код поддержки тем оставлен специально для тебя :)

function resetTheme() {
	setTheme('standart');
}

function setTheme(arg) {	
	var tvalue;
	if (arg == 'loadit') {
		//Загрузить
		try {
		tvalue = fs.readFileSync(gui.App.dataPath + '/NULLconfig.ini', 'utf-8');
		} catch(a) {
		tvalue = '';
		}
		setTheme(tvalue);
		//
	} else {
		//сбросить
		fs.writeFileSync(gui.App.dataPath + '/NULLconfig.ini', '');
		document.getElementById('themeHere').innerHTML = '';
		//
	}
}

function clearData(arg) {
	if (arg == 'cache') {
		fs.writeFileSync(gui.App.dataPath + '/Cache/index', '');
		var temp;
		for (var i = 0;;i++) {
			try {
				temp = fs.readFileSync(gui.App.dataPath + '/Cache/data_' + i, 'utf-8');
			} catch(a) {
				temp = null;
			}
			if (temp != null) {
				fs.writeFileSync(gui.App.dataPath + '/Cache/data_' + i, '');
			} else {
				break;
			}
		}
		alert('Для получения эффекта перезагрузите браузер!');
	}

	if (arg == 'сookie') {
		fs.writeFileSync(gui.App.dataPath + '/cookies', '');
		fs.writeFileSync(gui.App.dataPath + '/cookies-journal', '');
		alert('Для получения эффекта перезагрузите браузер!');
	}
}

